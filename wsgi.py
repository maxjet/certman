"""WSGI runner."""

import os
import sys

sys.path.append(os.path.dirname(__file__))

from app import app

def application(environ, start_response):
    return app(environ, start_response)
