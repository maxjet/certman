function sendCommand(){
    $('#spamer').empty().append('Ожидание ответа сервера...');
    $.post( "/command/exec", {val: $("#paramFromSellCommand" ).val()})
    .done(function( data ) {
        // if(data.result==true) $('#spamer').empty().append(data.msg)
        // else 
        s1 = data.msg==''?'':data.msg
        s2 = data.err==''?'':' ' + data.err
        $('#spamer').empty().append(s1+s2)
        console.log(data);
    });
};
function getText(namef){
    $.post( "/command/log", {val: namef})
    .done(function( data ) {
        $('#text_file_log').empty().html(data.data)
        console.log(data);
    });
};
$(function() {
    $('#goexec').click(()=>{
        sendCommand();
    })
    $('#paramFromSellCommand').keypress(function(event){
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if(keycode == '13'){
            sendCommand();
        }
    });
    $('.list-group-item').click(function(){
        $('.list-group-item').removeClass('active');
        getText($(this).html());
        $(this).addClass('active');

    })
    console.log( "ready!" );
});