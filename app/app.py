import os
from os import path

from flask import Flask, Blueprint, render_template
import logging  
import logging.config

from . import index
# import settings 

log_file_path = path.join(path.dirname(path.abspath(__file__)), 'logging.conf')
logging.config.fileConfig(log_file_path)

app = Flask(__name__)

app.logger.info('App start!')

env = ''
if app.env == 'testing':
   env = 'settings.TestingConfig'
elif app.env == 'production':
    env = 'settings.ProductionConfig'
else:
    env = 'settings.DevelopmentConfig'
app.config.from_object(os.environ.get('PROJECT_SETTINGS', env))

app.debug = True

passed = False

app.register_blueprint(index)
