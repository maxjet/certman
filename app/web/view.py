import os

from flask import Blueprint, render_template, redirect, url_for, jsonify, request, current_app, session, redirect

from ..util import shell
from ..models.ldapuser import check_AD_user, ldap_get_users
from ..models import Message, AppLdapUser, CurrentUser
from ..models.decorators import isauthorized, isnetgroups

index = Blueprint(
    'index', __name__,
    template_folder='templates',
    static_folder='static',
    url_prefix='/'
)

@index.route('/login', methods=['GET', 'POST'])
def login():
    if 'REMOTE_USER' in request.environ:
        session['username'] = request.environ['REMOTE_USER'].split('@')[0]
        return redirect(url_for('index.command'))
    if request.method == 'POST':
        login = request.form['email']
        paswd = request.form['password']
        if check_AD_user(login, paswd):
            session['username'] = login.split('@')[0]            
            return redirect(url_for('index.home'))
        else:
           return render_template('login.html',data = type('',(object,),{'err':'Введен неверный адрес электронной почты или пароль.', 'login':login, 'paswd':paswd})) 
    return render_template('login.html',data = type('',(object,),{'err':'', 'login':'', 'paswd':''}))

@index.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index.login'))

@index.route('/')
@isauthorized
def home():
    cur_user = CurrentUser(session['username']) 
    return render_template('index.html', name=cur_user.full_name) 

@index.route('/command/')
@isauthorized
def command():
    cur_user = CurrentUser(session['username'])    
    return render_template('commander.html', data = type('',(object,),{'name':cur_user.full_name, 'command':''}))

@index.route('/about/')
def about():
    cur_user = CurrentUser(session['username'])
    return render_template('about.html', name=cur_user.full_name)


@index.route('/logs/')
def logs():
    cur_user = CurrentUser(session['username'])
    arr = os.listdir(current_app.config.get('DIR_LOG', ''))
    return render_template('logs.html', data = type('',(object,),{'name':cur_user.full_name, 'files':arr}))

@index.route('/command/exec', methods=['POST'])
@isauthorized
@isnetgroups
def exec():    
    msg = Message()
    msg.current_user = session['username']
    data = request.form 
    if not data['val']:
        msg.err = 'Нет параметра'
    else:
        msg=shell.go_to_process(data['val'],session['username'])
    msg.send_loger()
    return jsonify(msg.to_json())
    
@index.route('/command/log', methods=['POST'])
@isauthorized
@isnetgroups
def log():    
    data = request.form 
    text=[]
    if data['val']:
        with open(current_app.config.get('DIR_LOG', '')+data['val'], encoding="utf-8") as f:
            lines = f.readlines()
            for line in lines:
                text.append('<p>'+line+'</p>')
    return jsonify({'data': ' '.join(text)})


@index.route('/getusers')
def getusers():
    name = request.args.get('name')
    data = ldap_get_users(name) 
    return jsonify(data)