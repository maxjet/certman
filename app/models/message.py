from flask import current_app
class Message(object):
    """ Message send to anyone """

    def __init__(self):
        self.result = False
        self.msg = ''
        self.code = ''
        self.err = ''
        self.current_user = 'ananimus'
    
    def set_val(self, result, msg, code, err, cur_user):
        self.result = result
        self.msg = msg
        self.code = code
        self.err = err
        self.current_user = cur_user
    
    def get_str(self):
        s= "user: %s, result: %s, code: %s, msg: %s, err: %s" % (self.current_user, str(self.result), self.code, self.msg, self.err)
        return s

    def send_loger(self):
        if self.result:
            current_app.logger.info(self.get_str())
        else:
            current_app.logger.warning(self.get_str())
    
    def to_json(self):
        return {
            "user": self.current_user,
            "result": self.result, 
            "code": self.code, 
            "msg": self.msg, 
            "err": self.err
        }

