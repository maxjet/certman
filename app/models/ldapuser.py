"""App services."""
from flask import current_app
from typing import List
from ldap3 import Server, Connection, ALL, SUBTREE

def  check_AD_user(_user, _paswd):
        ldap = AppLdapUser(current_app.config.get('LDAP_HOST', ''), _user, _paswd, current_app.config.get('LDAP_SEARCH_BASE', ''))
        return ldap.check_user()

def ldap_get_users(_search):
    ldap = AppLdapUser(current_app.config.get('LDAP_HOST', ''), current_app.config.get('LDAP_USER', ''), current_app.config.get('LDAP_PASSWORD', ''), current_app.config.get('LDAP_SEARCH_BASE', ''))
    return ldap.get_user(_search, False)

class AppLdapUser(object):
    """Сервис для работы с пользователями LDAP."""

    def __init__(self, server_name: str, user_name: str, user_password: str, ad_search_tree: str):
        """Инициализация."""
        self._ad_search_tree = ad_search_tree
        self._connection = Connection(Server(server_name, get_info=ALL), user=user_name, password=user_password)

    def get_user(self, search: str, one=True):
        users = []
        conn = self._connection
        if not conn.bind():
            print('error in bind', conn.result)
        conn.search(self._ad_search_tree, search_filter='(&(objectCategory=Person)(sAMAccountName={}))'.format(search), search_scope=SUBTREE, attributes=['cn', 'sAMAccountName', 'memberOf'])
        for entry in conn.entries:
            users.append({'id': entry['sAMAccountName'].value, 'name': entry['cn'].value, 'memberof': entry['memberOf'].value})
        conn.unbind()
        if len(users):
            if one: 
                return users[0]
            else: 
                return users
        else:
            return []


    def check_user(self):
        conn = self._connection
        if not conn.bind():
            return False
        conn.unbind()
        return True
    
   

