from flask import redirect, url_for, request, current_app, session, jsonify
from functools import wraps
from ..models import Message, CurrentUser

 
def isauthorized(func):
    @wraps(func)
    def wrapper():
        if 'username' in session: 
            return func()
        else:
            if 'REMOTE_USER' in request.environ:
                session['username'] = request.environ['REMOTE_USER'].split('@')[0]
                return func()
            else:
                if request.method == 'POST':
                    msg = Message()
                    msg.current_user = 'None'
                    msg.msg = 'Вы не авторизованы, в доступе отказано.'
                    msg.err = 'Not Authorized'
                    return jsonify(msg.to_json())
                else:
                    return redirect(url_for('index.login'))
    return wrapper

def isnetgroups(func):
    @wraps(func)
    def wrapper():
        msg = Message()
        msg.current_user = session['username']
        cur_user = CurrentUser(session['username'])
        if cur_user.boo_user_in_group():
            return func()
        else:
            msg.msg = 'У вас не достаточно прав для выполнения операции. Обратитесь к админитратору.'
            msg.err = 'Access is denied!'
            msg.send_loger()
            return jsonify(msg.to_json())
    return wrapper