""" Current user """
from flask import current_app, session

from .ldapuser import check_AD_user
from .ldapuser import AppLdapUser

class CurrentUser(object):

    def __init__(self, login):
        self.login = login
        self.full_name = 'ananimus'
        self.groups = [] 
        self.get_AD_user()   
    
    def get_AD_user(self):
        ldap = AppLdapUser(current_app.config.get('LDAP_HOST', ''), current_app.config.get('LDAP_USER', ''), current_app.config.get('LDAP_PASSWORD', ''), current_app.config.get('LDAP_SEARCH_BASE', '')) 
        ad_user = ldap.get_user(self.login)

        self.full_name = ad_user['name']
        if ad_user['memberof'] != None:
            for item in ad_user['memberof']:
                mixed = item.split(',')
                zol = list(map(lambda y: y.replace('CN=',''), list(filter(lambda x: x.find('CN=') != -1, mixed))))
                self.groups.append(zol[0])
        return ad_user

    def boo_user_in_group(self):
        result = False
        if current_app.config.get('ACL_GROUPS', '') == []:
            return True
        for item_group in current_app.config.get('ACL_GROUPS', ''):
            for self_item_group in self.groups:
                if item_group == self_item_group:
                    result = True
        return result
