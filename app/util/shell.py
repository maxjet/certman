import subprocess

from flask import current_app

from . import Message

def go_to_process(val, cur_user):
    cmd = current_app.config['SHELL_COMMAND'] + ' ' + val+"'"
    message = Message()
    proc = subprocess.call(cmd, shell = True)
    
    if val.find('*') ==-1:
        if not proc:
            message.set_val(True, 'Cleaning '+val+' ..Ok!', proc, '', cur_user)
        else:
            message.set_val(False, 'Cleaning '+val+'  ..Error', proc, 'Процесс вернул значение ошибки!', cur_user)
    else:
        message.set_val(False, val+'  ..Error', proc, 'Bad char', cur_user)
    
    return message

def sum(arg):
    total = 0
    for val in arg:
        total += val
    return total
