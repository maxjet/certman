"""Settings."""

import os, datetime

class BaseConfig(object):
    """Base configuration."""

    SHELL_COMMAND = "uname -a "  # Команда выполняемая на сервере (пробел в конце ставить не надо)
    ACL_GROUPS = ['gr-cpmo']   # Группы AD юзеру которых разрешено выполнять команды (пустой список - снятие ограничений на принадлежность к группам)

    DEBUG = False
    SECRET_KEY = "MY_VERY_SECRET_KEY"
    CSRF_ENABLED = True
    LDAP_HOST = ''
    LDAP_USER = ''
    LDAP_PASSWORD = ''
    LDAP_SEARCH_BASE = ''
    COOKIE_SECURE = 'Secure'                                                     
    # COOKIE_DURATION = datetime.timedelta(days=365)

    DIR_LOG = '/var/log/certman/'

    

class DevelopmentConfig(BaseConfig):
    """Development configuration."""

    DEBUG = True
    DEBUG_TB_PROFILER_ENABLED = True
    DEBUG_TB_INTERCEPT_REDIRECTS = False
    LDAP_HOST = '192.168.127.254'
    LDAP_USER = 'user@intm.ca'
    LDAP_PASSWORD = '*******'
    LDAP_SEARCH_BASE = 'DC=intm,dc=ca'    

class ProductionConfig(BaseConfig):
    """Production configuration."""

    LDAP_HOST = '10.2.200.29'
    LDAP_USER = 'scaner@rosgvard.ru'
    LDAP_PASSWORD = 'Win2008'
    LDAP_SEARCH_BASE = 'dc=rosgvard,dc=ru'

class TestingConfig(BaseConfig):
    """Testing configuration."""

    TESTING = False
